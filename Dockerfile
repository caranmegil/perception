FROM node:18-alpine3.14
RUN apk update && apk upgrade && sync
EXPOSE 3000
WORKDIR /usr/perception
COPY . /usr/perception
RUN npm ci
ENV NODE_ENV production
CMD ["npm", "start"]