/*
 index.js - The driver
 Copyright (C) 2022  William R. Moore <william@nerderium.com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import { json } from 'body-parser';
import express from 'express';
import 'dotenv/config';
import mongoose, { Schema } from 'mongoose';
import { subHours, subMinutes } from 'date-fns'
import { formatInTimeZone } from 'date-fns-tz';

mongoose.connect(process.env.DB_URL);

const TemperatoSchema = new Schema({
    timestamp: Schema.Types.String,
    value: Schema.Types.Number,
});

const RainfallSchema = new Schema({
    timestamp: Schema.Types.String,
    value: Schema.Types.Number,
});

const NinasGardenSchema = new Schema({
    timestamp: Schema.Types.String,
    moisture: Schema.Types.Number,
    temperature: Schema.Types.Number,
});

const purgeCollection = async (model: any, minutesInHist: number) => {
    const cutoffTime = formatInTimeZone(subMinutes(new Date(), minutesInHist), 'UTC', 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
    console.log(`Purging model before ${cutoffTime}`);
    await model.deleteMany({timestamp: {
        $lt: cutoffTime,
    }});
}

const TemperatoModel = mongoose.model('temperato', TemperatoSchema);
const RainfallModel = mongoose.model('rainfall', RainfallSchema);
const NinasGardenModel = mongoose.model('ninasgarden', NinasGardenSchema);

setInterval(() => {

    purgeCollection(TemperatoModel, 300);
    purgeCollection(RainfallModel, 1440);
    purgeCollection(NinasGardenModel, 1440);

}, 3600000)

const PORT = process.env.PORT || 3000;

(async () => {
    const app = express();

    app.use(
        '/',
        json(),
    );

    app.get('/:dataType', async (req, res) => {
        switch (req.params.dataType) {
            case 'temperato': {
                let temperature = 0.0;
                const now = new Date();
        
                TemperatoModel.find({}, ['value'], {
                    sort: {
                        timestamp: -1,
                    },
                }, (err: any, allTemps: any)=> {
                    if (allTemps && allTemps.length > 0) {
                        temperature = allTemps[0].value;
                        console.log(temperature);
                    }

                    res.send({
                        temperature,
                    })
                });

                break;
            }
            case 'rainfall24Hours': {
                let rainfall24Hours = 0.0;

                try {
                    const rainfallStartDate = formatInTimeZone(subHours(new Date, 24), 'UTC', 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                    const rainfallEndDate = formatInTimeZone(new Date(), 'UTC', 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                    RainfallModel.find({
                        timestamp: {
                            $gt: rainfallStartDate,
                            $lt: rainfallEndDate,
                        },
                    }, ['timestamp', 'value'], {
                        sort: {
                            timestamp: -1,
                        },
                    }, (err: any, data: any) => {

                        if (data && data.length > 0) {
                            rainfall24Hours = data.reduce((rainfall: number, dataPoint: any) => {
                                let rainfallValue = rainfall;
                                if (dataPoint.value) {
                                    rainfallValue += dataPoint.value;
                                }

                                return rainfallValue;
                            }, 0.);

                            res.send({
                                rainfall24Hours,
                            });
                        }
                    });
                } catch(e: any) {
                    console.error(e);
                }
                break;
            }
        }
    });

    app.post('/:dataType', (req, res) => {
        switch (req.params.dataType) {
            case 'temperato': {
                const tempData = new TemperatoModel();
                tempData.timestamp = formatInTimeZone(new Date(), 'UTC', 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                tempData.value = req.body.temperature;
                tempData.save();
                res.json({
                    temperature: req.body.temperature,
                });
                break;
            }
            case 'rainfall': {
                const rainfallData = new RainfallModel();
                rainfallData.timestamp = formatInTimeZone(new Date(), 'UTC', 'yyyy-MM-dd\'T\'HH:mm:ssXXX');
                rainfallData.value = req.body.value;
                rainfallData.save();
                res.json({
                    rainfall: req.body.value,
                });
                break;
            }
        }
    });

    const appServer = app.listen(PORT);
})();